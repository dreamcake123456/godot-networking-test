extends RichTextLabel

const max_lines = 10
const max_char_per_line = 30

var text_arr = [] 

func add(text:String):
	if text.length() > max_char_per_line:
		for i in slice_text(text):
			text_arr.append(i)
	else:
		text_arr.append(text)
	while text_arr.size() >= max_lines + 1:
		text_arr.pop_front()
	clear()
	for text in text_arr:
		add_text(text)
		newline()
		
func slice_text(long_text:String):
	var sliced_text_arr = []
		
	for i in range(30, long_text.length(), max_char_per_line):
		var txt = long_text.substr(i - max_char_per_line,
			max_char_per_line)
		sliced_text_arr.append(txt)
		
	return sliced_text_arr
