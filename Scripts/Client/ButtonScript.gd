extends Node

func _ready():
	$"Server".connect("button_up", self, "_as_server")
	$"Client".connect("button_up", self, "_as_client")

func _as_server():
	$"../NetworkManager".make_server()

func _as_client():
	$"../NetworkManager".make_client("127.0.0.1")
