############################################################
#   This script stores information to send to the server   #
#         when connected. These are mostly used for        #
#      distinguishing between more than one clients.       #
#        Can be safely ignored in case of duel games       #
############################################################
extends Node
class_name PlayerInfo

const my_name = "Player"
