extends Node

const PORT = 12345
const MAX_PLAYERS = 2

var self_network_id
var player_info = {}

onready var status_displayer = get_node("../StatusDisplay")

func make_server():
	#Register signals
	get_tree().connect("network_peer_connected", self, "_on_client_connect")
	get_tree().connect("network_peer_disconnected", self, "_on_client_disconnect")
	#Create network peers
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(PORT, MAX_PLAYERS)
	get_tree().set_network_peer(peer)
	#Server id is always 1
	self_network_id = 1
	#
	status_displayer.add("Server started")
	#Register myself
	player_info[1] = PlayerInfo.my_name
	status_displayer.add(PlayerInfo.my_name + " has joined the server")
	
func make_client(address):
	#Register signals
	get_tree().connect("connected_to_server", self, "_on_connect_success")
	get_tree().connect("connection_failed", self, "_on_connect_fail")
	get_tree().connect("server_disconnected", self, "_on_server_disconnect")
	#Create network peers
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(address, PORT)
	get_tree().set_network_peer(peer)

remotesync func pre_init():
	get_tree().set_pause(true)
	status_displayer.add("Initializing...")
	completed_init()
	
func completed_init():
	if get_tree().is_network_server():
		_on_client_load_complete(1)
	else:
		rpc_id(1, "_on_client_load_complete", self_network_id)

remotesync func post_init():
	get_tree().set_pause(false)
	status_displayer.add("Done!")

remotesync func register_player(info):
	var id = get_tree().get_rpc_sender_id()
	player_info[id] = info;
	status_displayer.add(info + " has joined the server")
	if player_info.size() == MAX_PLAYERS:
		rpc("pre_init")
	
remotesync func unregister_player(id):
	assert(id in player_info)
	status_displayer.add(player_info[id] + " has left the server")
	player_info.erase(id)
	if id in done_players:
		done_players.erase(id)

#####For Server Only#####
var done_players = []

#When someone successfully connected us
func _on_client_connect(id):
	pass

#When someone is disconnected from us
func _on_client_disconnect(id):
	rpc("unregister_player", id)
	
remote func _on_client_load_complete(id):
	#Checks
	assert(id in player_info, "Id is not in player_info! Unmatched id:" + str(id))
	assert(not id in done_players, "Id already exists in done_players! Duplicated id:" + str(id))
	
	done_players.append(id)
	if done_players.size() == player_info.size():
		rpc("post_init")



#####For Clients Only#####

#When we successfully connect to the server
func _on_connect_success():
	#Get my Uid from the network
	self_network_id = get_tree().get_network_unique_id()
	rpc("register_player", PlayerInfo.my_name)
	
#When we failed to connect to the server
func _on_connect_fail():
	pass
	
#When server is disconnected from us
func _on_server_disconnect():
	pass
